**proto objects - src/main/proto**  
Attendance.proto (Proto object for attendance table)  
  
---  
  
**csv data - src/resources/**   
csv files to populate attendanceData table  
  
---  

## CSVtoProto.java  

Creates serialized proto file from above mentioned csv  
Output path - resources/ProtoOutputFiles  
  
**creating tables**  
  
## UploadFiletoHDFS.java  
To store serialized proto file from local to HDFS  
  
## HbaseTableCreator.java  
To create Attendance table  
Uses Admin.createTable method  
  
## Main.java  
Calls UploadFiletoHDFS and HbaseTableCreator methods internally  
Contains main method  
  
**insert data into hbase tables**  
  
## MRDriver.java  
Driver class for mapreduce job for creating HFiles  
Calls bulkLoad method after job completion  
  
## HbaseMapper.java  
Writes data into Put object  
Filters out attendance data for every employee and saves the list of proto objects into one table row  
  
---  
  
## EmployeeAttendanceDriver.java  
Keeps map of employee Id to corresponding building code  
Mapper  
- Parses attendance data of each employee from hbase to proto object  
- Returns context with building code as key and list of (employees,attendance count) as value  	
Reducer  
- Finds minimum attendance employee for each building and writes into output directory  
- Output directory - resources/ProtoOutputFiles/Output  
**Mapper class - EmployeeAttendanceMapper.java  
Reducer class - EmployeeAttendanceReducer.java**  
  
---  
