import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import java.io.*;
import java.net.URI;
import util.Constants;
public class UploadFiletoHDFS {
    public static void main(String args[]) throws IOException {
        String localPath = Constants.AttendanceProtoOutputFile;
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(URI.create(Constants.uri), conf);
        Path output = new Path(Constants.Attendance_HDFS_OUTPUT_PATH);
        if (fs.exists(output)) {
            fs.delete(output, true);
        }
        fs.copyFromLocalFile(new Path(localPath), new Path(Constants.Attendance_HDFS_OUTPUT_PATH));

    }
}
